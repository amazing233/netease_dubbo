package com.kgc.cn.consumer.controller;

import com.alibaba.dubbo.common.utils.CollectionUtils;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kgc.cn.common.config.ReturnResult.ReturnResult;
import com.kgc.cn.common.dto.Employee;
import com.kgc.cn.common.enums.EmployeeEnum;
import com.kgc.cn.common.enums.ExcelEnum;
import com.kgc.cn.common.enums.MailEnum;
import com.kgc.cn.common.model.LoginParam;
import com.kgc.cn.common.service.ActivemqService;
import com.kgc.cn.common.service.AttendanceService;
import com.kgc.cn.common.service.EmployeeService;
import com.kgc.cn.common.util.ReturnResultUtils;
import com.kgc.cn.common.vo.EmployeeVo;
import com.kgc.cn.consumer.config.webMvcConfig.LoginEmployee;
import com.kgc.cn.consumer.config.webMvcConfig.LoginRequired;
import com.kgc.cn.consumer.model.ParamMsg;
import com.kgc.cn.consumer.model.ParamPwd;
import com.kgc.cn.consumer.util.RedisUtils;
import com.kgc.cn.consumer.util.ReturnPageUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Api(tags = "人事管理")
@RestController
@RequestMapping(value = "/employee")
public class EmployeeController {

    @Reference
    private EmployeeService employeeService;
    @Reference
    private AttendanceService attendanceService;
    @Reference
    private ActivemqService activemqService;
    @Autowired
    private RedisUtils redisUtils;

    /**
     * @param loginParam
     * @param request
     * @return
     */
    @ApiOperation("手机号或者邮箱登录")
    @PostMapping(value = "/loginByPhoneOrEmail")
    public ReturnResult loginByPhoneOrEmail(@Valid LoginParam loginParam, HttpServletRequest request) {
        String token = request.getSession().getId();
        if (StringUtils.isEmpty(loginParam.getPhoneOrEmail()))
            return ReturnResultUtils.returnFail(122, "用户的手机号或者邮箱不能为空");
        if (ObjectUtils.isEmpty(employeeService.seEmployeeByPhoneOrEmail(loginParam.getPhoneOrEmail())))
            return ReturnResultUtils.returnFail(122, "该账号对应的用户不存在");
        EmployeeVo employeeVo = employeeService.seEmployeeByPhoneOrEmailLogin(loginParam);
        if (ObjectUtils.isEmpty(employeeVo)) {
            return ReturnResultUtils.returnFail(1, "用户名或者密码错误");
        } else {
            setRedisEmployee(token, employeeVo);
            attendanceService.insertAttendance(employeeVo.getEId());
            return ReturnResultUtils.returnSuccess(token);
        }
    }

    public void setRedisEmployee(String token, EmployeeVo employeeVo) {
        Employee employee = Employee.builder().build();
        BeanUtils.copyProperties(employeeVo, employee);
        String employeeJsonStr = JSONObject.toJSONString(employee);
        redisUtils.set("employees:" + token, employeeJsonStr);
    }

    /**
     * 录入人员信息
     *
     * @return
     * @throws Exception
     */
    @ApiOperation(value = "excel表格录入人员信息")
    @LoginRequired
    @PostMapping(value = "/excelAdd")
    public ReturnResult excelAdd(@LoginEmployee Employee employee) throws Exception {
        if (employeeService.excelAddCredits()) {
            activemqService.sendMsgByQueue("sendMail", MailEnum.SendEmail.SEND_EMAIL.getTxt());
            return ReturnResultUtils.returnSuccess(ExcelEnum.ExcelTypes.SUCCESS);
        }
        return ReturnResultUtils.returnFail(ExcelEnum.ExcelTypes.FALSE.getCode(), ExcelEnum.ExcelTypes.FALSE.getMsg());
    }

    /**
     * 通过员工id
     * 或者姓名进行模糊搜索
     *
     * @param str
     * @return
     */
    @ApiOperation("通过员工id或者姓名进行搜索")
    @LoginRequired
    @GetMapping(value = "/searchEmployeeByIdOrName")
    public ReturnResult searchEmployeeByIdOrName(@LoginEmployee Employee employee,
                                                 @ApiParam(value = "请输入2") @RequestParam(name = "1") int pageNum,
                                                 @ApiParam(value = "请输入1") @RequestParam(name = "2") int pageSize,
                                                 @ApiParam(value = "请输入员工的id或者姓名") @RequestParam(name = "员工id或姓名") String str) {
        PageInfo<EmployeeVo> employeeVoList = employeeService.searchEmployeeByIdOrName(pageNum, pageSize, str);
        if (employeeVoList == null)
            return ReturnResultUtils.returnFail(1, "你查找的员工不存在");
        else {
            return ReturnResultUtils.returnSuccess(employeeVoList);
        }
    }

    /**
     * 通过员工入职时间段进行查询
     *
     * @param startTime
     * @param endTime
     * @return
     */
    @ApiOperation("通过员工入职时间段进行查询")
    @LoginRequired
    @PostMapping(value = "/saarchEmployeeByTime")
    public ReturnResult saarchEmployeeByTime(@LoginEmployee Employee employee,
                                             @ApiParam(value = "请输入开始时间") @RequestParam(name = "开始时间") String startTime,
                                             @ApiParam(value = "请输入结束时间") @RequestParam(name = "结束时间") String endTime) {
        List<EmployeeVo> employeeVoList = employeeService.searchEmployeeByJTime(startTime, endTime);

        return ReturnResultUtils.returnSuccess(employeeVoList);
    }

    /**
     * 删除员工
     *
     * @param employee
     * @param eId
     * @return
     */
    @ApiOperation("删除员工信息")
    @LoginRequired
    @GetMapping("/del")
    public ReturnResult delete(@LoginEmployee Employee employee,
                               @ApiParam(value = "输入删除员工id") @RequestParam(name = "删除的员工id") String eId) {
        return employeeService.delEmployee(employee, eId);
    }

    /**
     * 修改密码
     *
     * @param employee
     * @param paramPwd
     * @return
     */
    @ApiOperation("修改密码")
    @LoginRequired
    @PostMapping("/updatepassword")
    public ReturnResult updatepassword(@LoginEmployee Employee employee, @Valid ParamPwd paramPwd) {
        if (paramPwd.getePassword().isEmpty()) {
            return ReturnResultUtils.returnFail(EmployeeEnum.EmployeeError.EmployeeEnum_GENGXINZHIYUAN2_ERROR.getCode(),
                    EmployeeEnum.EmployeeError.EmployeeEnum_GENGXINZHIYUAN2_ERROR.getMsg());
        }
        BeanUtils.copyProperties(paramPwd, employee);
        return employeeService.updateEmployee(employee);

    }

    /**
     * 修改个人信息
     *
     * @param employee
     * @param paramMsg
     * @return
     */
    @ApiOperation("修改个人信息")
    @LoginRequired
    @PostMapping("/updatemsg")
    public ReturnResult updatemsg(@LoginEmployee Employee employee, @Valid ParamMsg paramMsg) {
        BeanUtils.copyProperties(paramMsg, employee);
        return employeeService.updateEmployee(employee);
    }

}
