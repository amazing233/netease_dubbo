package com.kgc.cn.provider.serviceImpl;

import com.alibaba.dubbo.config.annotation.Service;
import com.kgc.cn.common.service.ActivemqService;
import com.kgc.cn.common.service.EmployeeService;
import com.kgc.cn.provider.utils.ActiveMqUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;

@Service
public class ActiveMqServiceImpl implements ActivemqService {

    @Autowired
    private ActiveMqUtil activeMqUtil;

    @Autowired
    private EmployeeService employeeService;

    @Override
    public void sendMsgByQueue(String name, Object o) {
        activeMqUtil.sendMsg(name, o);
    }

    @JmsListener(destination = "sendMail")
    public void sendMails() {
        employeeService.sendMailByMail();
    }


}
