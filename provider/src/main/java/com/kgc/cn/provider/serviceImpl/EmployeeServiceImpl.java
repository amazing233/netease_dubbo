package com.kgc.cn.provider.serviceImpl;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.excel.util.CollectionUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.google.common.collect.Lists;
import com.kgc.cn.common.config.ReturnResult.ReturnResult;
import com.kgc.cn.common.dto.Employee;
import com.kgc.cn.common.enums.EmployeeEnum;
import com.kgc.cn.common.enums.MailEnum;
import com.kgc.cn.common.model.LoginParam;
import com.kgc.cn.common.service.EmployeeService;
import com.kgc.cn.common.util.ReturnResultUtils;
import com.kgc.cn.common.vo.EmployeeVo;
import com.kgc.cn.provider.mapper.EmployeeMapper;
import com.kgc.cn.provider.utils.MailUtil;
import com.kgc.cn.provider.utils.PowerUtils;
import com.kgc.cn.provider.utils.ReadExcelUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Autowired
    private MailUtil mailUtil;

    @Override
    public boolean excelAddCredits() throws IOException {
        List<Employee> employeeLists = ReadExcelUtil.ReadEmployeeExcel();
        if (!CollectionUtils.isEmpty(employeeLists)) {
            employeeLists.forEach(employee -> {
                employeeMapper.insertSelective(employee);
            });
            return true;
        }
        return false;
    }

    /**
     * 修改员工信息
     *
     * @param employee
     * @return
     */
    @Override
    public ReturnResult updateEmployee(Employee employee) {
        //验证员工是否存在
        if (null != employeeMapper.selectByPrimaryKey(employee.getEId())) {
            //修改员工信息
            employeeMapper.updateByPrimaryKey(employee);
            return ReturnResultUtils.returnSuccess(EmployeeEnum.EmployeeTrue.EmployeeEnum_GENGXINZHIYUAN_TRUE.getMsg());
        } else {
            return ReturnResultUtils.returnFail(EmployeeEnum.EmployeeError.EmployeeEnum_GENGXINZHIYUAN_ERROR.getCode(),
                    EmployeeEnum.EmployeeError.EmployeeEnum_GENGXINZHIYUAN_ERROR.getMsg());
        }
    }

    /**
     * 删除职员
     *
     * @param employee
     * @param eId
     * @return
     */
    @Override
    public ReturnResult delEmployee(Employee employee, String eId) {
        PowerUtils powerUtils = new PowerUtils();
        //判断用户是否存在
        if (null != employeeMapper.selectByPrimaryKey(eId)) {
            //判断部门
            if (employee.getDId() == 1 || employee.getDId() == 2) {
                //判断是否越权
                if (employee.getEPower() > employeeMapper.selectByPrimaryKey(eId).getEPower()) {
                    //删除
                    employeeMapper.deleteByPrimaryKey(eId);
                    return ReturnResultUtils.returnSuccess(EmployeeEnum.EmployeeTrue.EmployeeEnum_SHANCHUZHIYUAN_TRUE.getMsg());
                }
                return ReturnResultUtils.returnFail(EmployeeEnum.EmployeeError.EmployeeEnum_SHANCHUZHIYUAN2_ERROR.getCode(),
                        EmployeeEnum.EmployeeError.EmployeeEnum_SHANCHUZHIYUAN2_ERROR.getMsg());
            }
            return ReturnResultUtils.returnFail(EmployeeEnum.EmployeeError.EmployeeEnum_SHANCHUZHIYUAN1_ERROR.getCode(),
                    EmployeeEnum.EmployeeError.EmployeeEnum_SHANCHUZHIYUAN1_ERROR.getMsg());
        }
        return ReturnResultUtils.returnFail(EmployeeEnum.EmployeeError.EmployeeEnum_GENGXINZHIYUAN_ERROR.getCode(),
                EmployeeEnum.EmployeeError.EmployeeEnum_GENGXINZHIYUAN_ERROR.getMsg());
    }

    /**
     * 职员根据手机号或者邮箱以及密码登录查询
     *
     * @param loginParam
     * @return
     */
    @Override
    public EmployeeVo seEmployeeByPhoneOrEmailLogin(LoginParam loginParam) {
        Employee employee = employeeMapper.selectByPhoneOrEmailLogin(loginParam.getPhoneOrEmail(), loginParam.getPassword());
        EmployeeVo employeeVo = new EmployeeVo();
        BeanUtils.copyProperties(employee, employeeVo);
        return employeeVo;
    }

    /**
     * 根据手机号以及邮箱判断有无员工
     *
     * @param str
     * @return
     */
    @Override
    public EmployeeVo seEmployeeByPhoneOrEmail(String str) {
        Employee employee = employeeMapper.selectByPhoneOrEmail(str);
        EmployeeVo employeeVo = new EmployeeVo();
        BeanUtils.copyProperties(employee, employeeVo);
        return employeeVo;
    }

    /**
     * 根据id以及姓名模糊查询员工
     *
     * @param eStr
     * @return
     */
    @Override
    public PageInfo<EmployeeVo> searchEmployeeByIdOrName(int pageNum, int pageSize, String eStr) {
        PageHelper.startPage(pageNum, pageSize);
        Employee employee = Employee.builder().build();
        List<Employee> employeeList = employeeMapper.selectByIdOrName(eStr);
        List<EmployeeVo> employeeVoList = Lists.newArrayList();
        employeeList.forEach(employee1 -> {
            EmployeeVo employeeVo = new EmployeeVo();
            BeanUtils.copyProperties(employee1, employeeVo);
            employeeVoList.add(employeeVo);
        });
        PageInfo<EmployeeVo> pageInfo = new PageInfo<>(employeeVoList);
        return pageInfo;
    }

    /**
     * 根据入职时间段搜索员工
     *
     * @param startTime
     * @param endTime
     * @return
     */
    @Override
    public List<EmployeeVo> searchEmployeeByJTime(String startTime, String endTime) {
        List<Employee> employeeList = employeeMapper.searchEmployeeByJTime(startTime, endTime);
        List<EmployeeVo> employeeVoList = Lists.newArrayList();
        employeeList.forEach(employee -> {
            EmployeeVo employeeVo = new EmployeeVo();
            BeanUtils.copyProperties(employee, employeeVo);
            employeeVoList.add(employeeVo);
        });
        return employeeVoList;
    }

    /**
     * 消息中间件发邮件
     */
    @Override
    public void sendMailByMail() {
        List<Employee> employeeLists = employeeMapper.selectAll();
        employeeLists.forEach(employee -> {
            mailUtil.sendMail(employee.getEEmail(), MailEnum.SendEmail.SEND_EMAIL.getTxt());
        });
    }


}
