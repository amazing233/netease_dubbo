package com.kgc.cn.common.enums;

/**
 * Created by swj on 2019/12/5.
 */

//正确提示，持续添加中
public enum MessageTrue {

    MESSAGE_DENGLU_TRUE("查看商品成功"),
    MESSAGE_ZHUCE_TRUE("注册成功"),
    MESSAGE_UPDATE_TRUE("更新成功"),
    MESSAGE_ZHUXIAO_TRUE("注销成功"),
    MESSAGE_TUICHU_TRUE("退出成功"),
    MESSAGE_GENGXIN_TRUE("更新商品成功"),
    MESSAGE_DINGDAN_TRUE("订单生成成功"),
    MESSAGE_ZHIFU_TRUE("支付成功"),
    MESSAGE_SHANCHU_TRUE("删除商品成功"),
    MESSAGE_ZENJIA_TRUE("添加促销商品成功"),
    MESSAGE_CUXIAOSHAN_TRUE("删除促销商品成功");

    private String msg;

    MessageTrue(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }
}
