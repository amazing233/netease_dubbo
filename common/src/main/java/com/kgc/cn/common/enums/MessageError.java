package com.kgc.cn.common.enums;

/**
 * Created by swj on 2019/12/5.
 */
//错误提示..持续添加中
public enum MessageError {

    MESSAGE_ZHUCE_ERROR(101, "注册失败"),
    MESSAGE_DENGLU_ERROR(102, "登录失败"),
    MESSAGE_UPDATE_ERROR(103, "更新失败"),
    MESSAGE_ZHUXIAO_ERROR(104, "注销失败"),
    MESSAGE_TUICHU_ERROR(105, "退出失败"),
    MESSAGE_GENGXIN_ERROR(106, "更新商品错误，请检查商品Id"),
    MSEEAGE_DINGDAN_ERROR(107, "订单生成失败,请检查您的用户ID"),
    MSEEAGE_SHANCHU_ERROR(108, "删除商品失败，请检查您输入的商品ID"),
    MESSAGE_BUMEN_ERROR(109, "您没有操作此功能的权限"),
    MESSAGE_QUANXIAN_ERROR(110, "请通知管理员进行此操作"),
    MSSAGE_ZENJIA_ERROR(111, "增加商品失败，请检查商品id");

    private int code;
    private String msg;

    MessageError(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

}
