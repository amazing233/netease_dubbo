package com.kgc.cn.common.dto;

import java.util.ArrayList;
import java.util.List;

public class PromotionExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PromotionExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andPIdIsNull() {
            addCriterion("p_id is null");
            return (Criteria) this;
        }

        public Criteria andPIdIsNotNull() {
            addCriterion("p_id is not null");
            return (Criteria) this;
        }

        public Criteria andPIdEqualTo(Long value) {
            addCriterion("p_id =", value, "pId");
            return (Criteria) this;
        }

        public Criteria andPIdNotEqualTo(Long value) {
            addCriterion("p_id <>", value, "pId");
            return (Criteria) this;
        }

        public Criteria andPIdGreaterThan(Long value) {
            addCriterion("p_id >", value, "pId");
            return (Criteria) this;
        }

        public Criteria andPIdGreaterThanOrEqualTo(Long value) {
            addCriterion("p_id >=", value, "pId");
            return (Criteria) this;
        }

        public Criteria andPIdLessThan(Long value) {
            addCriterion("p_id <", value, "pId");
            return (Criteria) this;
        }

        public Criteria andPIdLessThanOrEqualTo(Long value) {
            addCriterion("p_id <=", value, "pId");
            return (Criteria) this;
        }

        public Criteria andPIdIn(List<Long> values) {
            addCriterion("p_id in", values, "pId");
            return (Criteria) this;
        }

        public Criteria andPIdNotIn(List<Long> values) {
            addCriterion("p_id not in", values, "pId");
            return (Criteria) this;
        }

        public Criteria andPIdBetween(Long value1, Long value2) {
            addCriterion("p_id between", value1, value2, "pId");
            return (Criteria) this;
        }

        public Criteria andPIdNotBetween(Long value1, Long value2) {
            addCriterion("p_id not between", value1, value2, "pId");
            return (Criteria) this;
        }

        public Criteria andGIdIsNull() {
            addCriterion("g_id is null");
            return (Criteria) this;
        }

        public Criteria andGIdIsNotNull() {
            addCriterion("g_id is not null");
            return (Criteria) this;
        }

        public Criteria andGIdEqualTo(String value) {
            addCriterion("g_id =", value, "gId");
            return (Criteria) this;
        }

        public Criteria andGIdNotEqualTo(String value) {
            addCriterion("g_id <>", value, "gId");
            return (Criteria) this;
        }

        public Criteria andGIdGreaterThan(String value) {
            addCriterion("g_id >", value, "gId");
            return (Criteria) this;
        }

        public Criteria andGIdGreaterThanOrEqualTo(String value) {
            addCriterion("g_id >=", value, "gId");
            return (Criteria) this;
        }

        public Criteria andGIdLessThan(String value) {
            addCriterion("g_id <", value, "gId");
            return (Criteria) this;
        }

        public Criteria andGIdLessThanOrEqualTo(String value) {
            addCriterion("g_id <=", value, "gId");
            return (Criteria) this;
        }

        public Criteria andGIdLike(String value) {
            addCriterion("g_id like", value, "gId");
            return (Criteria) this;
        }

        public Criteria andGIdNotLike(String value) {
            addCriterion("g_id not like", value, "gId");
            return (Criteria) this;
        }

        public Criteria andGIdIn(List<String> values) {
            addCriterion("g_id in", values, "gId");
            return (Criteria) this;
        }

        public Criteria andGIdNotIn(List<String> values) {
            addCriterion("g_id not in", values, "gId");
            return (Criteria) this;
        }

        public Criteria andGIdBetween(String value1, String value2) {
            addCriterion("g_id between", value1, value2, "gId");
            return (Criteria) this;
        }

        public Criteria andGIdNotBetween(String value1, String value2) {
            addCriterion("g_id not between", value1, value2, "gId");
            return (Criteria) this;
        }

        public Criteria andPPriceIsNull() {
            addCriterion("p_price is null");
            return (Criteria) this;
        }

        public Criteria andPPriceIsNotNull() {
            addCriterion("p_price is not null");
            return (Criteria) this;
        }

        public Criteria andPPriceEqualTo(Double value) {
            addCriterion("p_price =", value, "pPrice");
            return (Criteria) this;
        }

        public Criteria andPPriceNotEqualTo(Double value) {
            addCriterion("p_price <>", value, "pPrice");
            return (Criteria) this;
        }

        public Criteria andPPriceGreaterThan(Double value) {
            addCriterion("p_price >", value, "pPrice");
            return (Criteria) this;
        }

        public Criteria andPPriceGreaterThanOrEqualTo(Double value) {
            addCriterion("p_price >=", value, "pPrice");
            return (Criteria) this;
        }

        public Criteria andPPriceLessThan(Double value) {
            addCriterion("p_price <", value, "pPrice");
            return (Criteria) this;
        }

        public Criteria andPPriceLessThanOrEqualTo(Double value) {
            addCriterion("p_price <=", value, "pPrice");
            return (Criteria) this;
        }

        public Criteria andPPriceIn(List<Double> values) {
            addCriterion("p_price in", values, "pPrice");
            return (Criteria) this;
        }

        public Criteria andPPriceNotIn(List<Double> values) {
            addCriterion("p_price not in", values, "pPrice");
            return (Criteria) this;
        }

        public Criteria andPPriceBetween(Double value1, Double value2) {
            addCriterion("p_price between", value1, value2, "pPrice");
            return (Criteria) this;
        }

        public Criteria andPPriceNotBetween(Double value1, Double value2) {
            addCriterion("p_price not between", value1, value2, "pPrice");
            return (Criteria) this;
        }

        public Criteria andPDiscountIsNull() {
            addCriterion("p_discount is null");
            return (Criteria) this;
        }

        public Criteria andPDiscountIsNotNull() {
            addCriterion("p_discount is not null");
            return (Criteria) this;
        }

        public Criteria andPDiscountEqualTo(Double value) {
            addCriterion("p_discount =", value, "pDiscount");
            return (Criteria) this;
        }

        public Criteria andPDiscountNotEqualTo(Double value) {
            addCriterion("p_discount <>", value, "pDiscount");
            return (Criteria) this;
        }

        public Criteria andPDiscountGreaterThan(Double value) {
            addCriterion("p_discount >", value, "pDiscount");
            return (Criteria) this;
        }

        public Criteria andPDiscountGreaterThanOrEqualTo(Double value) {
            addCriterion("p_discount >=", value, "pDiscount");
            return (Criteria) this;
        }

        public Criteria andPDiscountLessThan(Double value) {
            addCriterion("p_discount <", value, "pDiscount");
            return (Criteria) this;
        }

        public Criteria andPDiscountLessThanOrEqualTo(Double value) {
            addCriterion("p_discount <=", value, "pDiscount");
            return (Criteria) this;
        }

        public Criteria andPDiscountIn(List<Double> values) {
            addCriterion("p_discount in", values, "pDiscount");
            return (Criteria) this;
        }

        public Criteria andPDiscountNotIn(List<Double> values) {
            addCriterion("p_discount not in", values, "pDiscount");
            return (Criteria) this;
        }

        public Criteria andPDiscountBetween(Double value1, Double value2) {
            addCriterion("p_discount between", value1, value2, "pDiscount");
            return (Criteria) this;
        }

        public Criteria andPDiscountNotBetween(Double value1, Double value2) {
            addCriterion("p_discount not between", value1, value2, "pDiscount");
            return (Criteria) this;
        }

        public Criteria andPStartTimeIsNull() {
            addCriterion("p_start_time is null");
            return (Criteria) this;
        }

        public Criteria andPStartTimeIsNotNull() {
            addCriterion("p_start_time is not null");
            return (Criteria) this;
        }

        public Criteria andPStartTimeEqualTo(String value) {
            addCriterion("p_start_time =", value, "pStartTime");
            return (Criteria) this;
        }

        public Criteria andPStartTimeNotEqualTo(String value) {
            addCriterion("p_start_time <>", value, "pStartTime");
            return (Criteria) this;
        }

        public Criteria andPStartTimeGreaterThan(String value) {
            addCriterion("p_start_time >", value, "pStartTime");
            return (Criteria) this;
        }

        public Criteria andPStartTimeGreaterThanOrEqualTo(String value) {
            addCriterion("p_start_time >=", value, "pStartTime");
            return (Criteria) this;
        }

        public Criteria andPStartTimeLessThan(String value) {
            addCriterion("p_start_time <", value, "pStartTime");
            return (Criteria) this;
        }

        public Criteria andPStartTimeLessThanOrEqualTo(String value) {
            addCriterion("p_start_time <=", value, "pStartTime");
            return (Criteria) this;
        }

        public Criteria andPStartTimeLike(String value) {
            addCriterion("p_start_time like", value, "pStartTime");
            return (Criteria) this;
        }

        public Criteria andPStartTimeNotLike(String value) {
            addCriterion("p_start_time not like", value, "pStartTime");
            return (Criteria) this;
        }

        public Criteria andPStartTimeIn(List<String> values) {
            addCriterion("p_start_time in", values, "pStartTime");
            return (Criteria) this;
        }

        public Criteria andPStartTimeNotIn(List<String> values) {
            addCriterion("p_start_time not in", values, "pStartTime");
            return (Criteria) this;
        }

        public Criteria andPStartTimeBetween(String value1, String value2) {
            addCriterion("p_start_time between", value1, value2, "pStartTime");
            return (Criteria) this;
        }

        public Criteria andPStartTimeNotBetween(String value1, String value2) {
            addCriterion("p_start_time not between", value1, value2, "pStartTime");
            return (Criteria) this;
        }

        public Criteria andPEndTimeIsNull() {
            addCriterion("p_end_time is null");
            return (Criteria) this;
        }

        public Criteria andPEndTimeIsNotNull() {
            addCriterion("p_end_time is not null");
            return (Criteria) this;
        }

        public Criteria andPEndTimeEqualTo(String value) {
            addCriterion("p_end_time =", value, "pEndTime");
            return (Criteria) this;
        }

        public Criteria andPEndTimeNotEqualTo(String value) {
            addCriterion("p_end_time <>", value, "pEndTime");
            return (Criteria) this;
        }

        public Criteria andPEndTimeGreaterThan(String value) {
            addCriterion("p_end_time >", value, "pEndTime");
            return (Criteria) this;
        }

        public Criteria andPEndTimeGreaterThanOrEqualTo(String value) {
            addCriterion("p_end_time >=", value, "pEndTime");
            return (Criteria) this;
        }

        public Criteria andPEndTimeLessThan(String value) {
            addCriterion("p_end_time <", value, "pEndTime");
            return (Criteria) this;
        }

        public Criteria andPEndTimeLessThanOrEqualTo(String value) {
            addCriterion("p_end_time <=", value, "pEndTime");
            return (Criteria) this;
        }

        public Criteria andPEndTimeLike(String value) {
            addCriterion("p_end_time like", value, "pEndTime");
            return (Criteria) this;
        }

        public Criteria andPEndTimeNotLike(String value) {
            addCriterion("p_end_time not like", value, "pEndTime");
            return (Criteria) this;
        }

        public Criteria andPEndTimeIn(List<String> values) {
            addCriterion("p_end_time in", values, "pEndTime");
            return (Criteria) this;
        }

        public Criteria andPEndTimeNotIn(List<String> values) {
            addCriterion("p_end_time not in", values, "pEndTime");
            return (Criteria) this;
        }

        public Criteria andPEndTimeBetween(String value1, String value2) {
            addCriterion("p_end_time between", value1, value2, "pEndTime");
            return (Criteria) this;
        }

        public Criteria andPEndTimeNotBetween(String value1, String value2) {
            addCriterion("p_end_time not between", value1, value2, "pEndTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}