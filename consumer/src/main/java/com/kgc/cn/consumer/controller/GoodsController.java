package com.kgc.cn.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.kgc.cn.common.config.ReturnResult.ReturnResult;
import com.kgc.cn.common.dto.Employee;
import com.kgc.cn.common.enums.ExcelEnum;
import com.kgc.cn.common.service.GoodsService;
import com.kgc.cn.common.util.ReturnResultUtils;
import com.kgc.cn.consumer.config.webMvcConfig.LoginEmployee;
import com.kgc.cn.consumer.config.webMvcConfig.LoginRequired;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "商品管理")
@RestController
@RequestMapping
public class GoodsController {

    @Reference
    private GoodsService goodsService;

    @ApiOperation(value = "读取excel列表并存入数据库")
    @LoginRequired
    @PostMapping(value = "/excelAdd")
    public ReturnResult excelAdd(@LoginEmployee Employee employee) throws Exception {
        if (goodsService.excelAddGoods()) {
            return ReturnResultUtils.returnSuccess(ExcelEnum.ExcelTypes.SUCCESS);
        }
        return ReturnResultUtils.returnFail(ExcelEnum.ExcelTypes.FALSE.getCode(), ExcelEnum.ExcelTypes.FALSE.getMsg());
    }


}
