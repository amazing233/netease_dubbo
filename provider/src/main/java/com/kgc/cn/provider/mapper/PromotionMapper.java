package com.kgc.cn.provider.mapper;

import com.kgc.cn.common.dto.Promotion;
import com.kgc.cn.common.dto.PromotionExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PromotionMapper {
    long countByExample(PromotionExample example);

    int deleteByExample(PromotionExample example);

    int deleteByPrimaryKey(Long pId);

    int insert(Promotion record);

    int insertSelective(Promotion record);

    List<Promotion> selectByExample(PromotionExample example);

    Promotion selectByPrimaryKey(Long pId);

    int updateByExampleSelective(@Param("record") Promotion record, @Param("example") PromotionExample example);

    int updateByExample(@Param("record") Promotion record, @Param("example") PromotionExample example);

    int updateByPrimaryKeySelective(Promotion record);

    boolean updateByPrimaryKey(Promotion record);

    //删除
    int delByTimeOut();

    /**
     * 根据商品id查找是否存在该折扣商品
     *
     * @param goodsId
     * @return
     */
    Promotion selectByGoodsId(@Param("goodsId") String goodsId);

}