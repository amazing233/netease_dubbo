package com.kgc.cn.provider.utils;

import com.kgc.cn.common.dto.Employee;
import com.kgc.cn.common.vo.EmployeeVo;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * Created by boot on 2019/12/13
 * 用户权限统一工具类
 */
@Component
public class PowerUtils {
    /**
     * 判断部门：1、管理部门  2、人事部门  3、销售部门
     */
    public static int JudgeDepartment(Employee employee) {
        return employee.getDId();
    }


    /**
     * 判断权限：1、普通用户  2、部门管理员  3、超级管理员
     */
    public static int JudgePower(Employee employee) {
        return employee.getEPower();
    }

}