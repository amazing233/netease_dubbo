package com.kgc.cn.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.kgc.cn.common.config.ReturnResult.ReturnResult;
import com.kgc.cn.common.dto.Employee;
import com.kgc.cn.common.service.AttendanceService;
import com.kgc.cn.common.util.ReturnResultUtils;
import com.kgc.cn.common.vo.EmployeeParamVo;
import com.kgc.cn.consumer.config.webMvcConfig.LoginEmployee;
import com.kgc.cn.consumer.config.webMvcConfig.LoginRequired;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@Api(tags = "首页模块")
@RequestMapping(value = "homePage")
public class HomePageController {

    @Reference
    private AttendanceService attendanceService;

    /**
     * 展示未出勤的员工的信息
     *
     * @param dateStr
     * @return
     */
    @ApiOperation("查询某天未出勤员工信息")
    @GetMapping(value = "/showNoAttendance")
    public ReturnResult showNoAttendance(String dateStr) {
        List<EmployeeParamVo> employeeParamVoList = attendanceService.searchNoAttendance(dateStr);
        if (CollectionUtils.isEmpty(employeeParamVoList)) {
            ReturnResultUtils.returnFail(10, "没有员工缺勤");
        }
        return ReturnResultUtils.returnSuccess(employeeParamVoList);
    }

    @ApiOperation("当前员工的出勤当月出勤天数")
    @LoginRequired
    @GetMapping(value = "EmployeeAttendanceState")
    public ReturnResult<String> EmployeeAttendanceState(@LoginEmployee Employee employee) {
        int num = attendanceService.daysOfAttendance(employee.getEId());
        String str = "当月出勤的天数为:" + num;
        return ReturnResultUtils.returnSuccess(str);
    }
}
