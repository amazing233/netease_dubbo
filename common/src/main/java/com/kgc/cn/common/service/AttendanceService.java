package com.kgc.cn.common.service;

import com.kgc.cn.common.vo.EmployeeParamVo;

import java.util.List;

public interface AttendanceService {

    /**
     * 实现员工打卡增加数据
     *
     * @param eId
     * @return
     */
    boolean insertAttendance(String eId);

    /**
     * 查询未出勤的员工
     * 展示id，名字，部门。
     *
     * @return
     */
    List<EmployeeParamVo> searchNoAttendance(String dateStr);

    /**
     * 查询当天员工当月出勤的天数
     *
     * @param eId
     * @return
     */
    int daysOfAttendance(String eId);
}
