package com.kgc.cn.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

public interface ExcelEnum {

    @Getter
    @AllArgsConstructor
    enum ExcelTypes implements ExcelEnum {
        SUCCESS(200, "信息录入成功"),
        FALSE(201, "信息录入异常");
        int code;
        String msg;
    }
}
