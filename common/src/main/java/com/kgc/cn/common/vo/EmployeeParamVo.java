package com.kgc.cn.common.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class EmployeeParamVo implements Serializable {
    private String eId;

    private String eName;

    private String dName;
}
