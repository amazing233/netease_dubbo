package com.kgc.cn.consumer.model;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * Created by boot on 2019/12/13
 */
@Api
public class ParamPwd implements Serializable {
    private static final long serialVersionUID = 7977194275356111895L;
    //员工密码
    @ApiModelProperty("输入要修改的密码")
    private String ePassword;

    public String getePassword() {
        return ePassword;
    }

    public void setePassword(String ePassword) {
        this.ePassword = ePassword;
    }


}