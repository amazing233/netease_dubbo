package com.kgc.cn.common.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class Goods implements Serializable {
    private String gId;

    private String gName;

    private Double gPrice;

    private String gType;

    private String gContent;

    private Long gSales;

}