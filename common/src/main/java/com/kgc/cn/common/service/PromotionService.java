package com.kgc.cn.common.service;

import com.kgc.cn.common.config.ReturnResult.ReturnResult;
import com.kgc.cn.common.vo.PromotionVo;

import java.util.List;

public interface PromotionService {

    /**
     * 批量增加促销商品
     *
     * @param discountGoodsId
     * @param content
     * @param startTime
     * @param endTime
     * @return
     */
    boolean addPromotionGoods(String discountGoodsId, double content, String startTime, String endTime);

    /**
     * 判断折扣以及折扣日期
     *
     * @param count
     * @param endTime
     * @return
     */
    boolean judge(double count, String endTime);


    /**
     * 删除商品
     *
     * @param
     * @return
     */
    ReturnResult delDisCommodity();

    /**
     * 修改促销商品信息
     *
     * @param promotionVo
     * @return
     */
    ReturnResult updatePromotion(PromotionVo promotionVo);

    /**
     * 根据名字查询促销商品
     *
     * @param Name
     * @return
     */
    ReturnResult<List<PromotionVo>> selPromotion(String Name);


}
