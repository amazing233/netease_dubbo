package com.kgc.cn.common.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class Department implements Serializable {
    private Long dId;

    private String dName;

}