package com.kgc.cn.provider.utils;

import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;

import javax.jms.Queue;
import javax.jms.Topic;

@Component
public class ActiveMqUtil {

    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    //发送消息
    public void sendMsg(String name, Object o) {
        Queue queue = new ActiveMQQueue(name);
        jmsMessagingTemplate.convertAndSend(name, o);
    }

    //topic订阅模式
    public void sendMsgByTopic(String name, Object o) {
        Topic topic = new ActiveMQTopic(name);
        jmsMessagingTemplate.convertAndSend(topic, o);
    }

}
