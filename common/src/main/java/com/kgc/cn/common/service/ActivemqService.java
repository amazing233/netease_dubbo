package com.kgc.cn.common.service;

public interface ActivemqService {
    /**
     * 中间件发送消息
     *
     * @param name
     * @param obj
     */
    void sendMsgByQueue(String name, Object obj);

}
