package com.kgc.cn.common.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class PromotionVo implements Serializable {
    private static final long serialVersionUID = 5455141209857663481L;
    private Long pId;

    private String gId;

    private Double pPrice;

    private Double pDiscount;

    private String pStartTime;

    private String pEndTime;

    private String gName;


}