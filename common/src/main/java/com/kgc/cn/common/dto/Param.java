package com.kgc.cn.common.dto;

import lombok.Data;

/**
 * Created by boot on 2019/12/18
 */
@Data
public class Param {
    private String gName;

    private String gContent;
}
