package com.kgc.cn.provider.serviceImpl;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSONObject;
import com.kgc.cn.common.config.ReturnResult.ReturnResult;
import com.kgc.cn.common.dto.Employee;
import com.kgc.cn.common.dto.Goods;
import com.kgc.cn.common.dto.GoodsExample;
import com.kgc.cn.common.enums.MessageError;
import com.kgc.cn.common.enums.MessageTrue;
import com.kgc.cn.common.service.GoodsService;
import com.kgc.cn.common.util.ReturnResultUtils;
import com.kgc.cn.common.vo.EmployeeVo;
import com.kgc.cn.common.vo.GoodsVo;
import com.kgc.cn.provider.mapper.GoodsMapper;
import com.kgc.cn.provider.utils.PowerUtils;
import com.kgc.cn.provider.utils.ReadExcelUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.List;

@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsMapper goodsMapper;

    @Autowired
    private PowerUtils powerUtils;

    @Override
    public boolean excelAddGoods() throws IOException {
        List<Goods> goodsLists = ReadExcelUtil.ReadGoodsExcel();
        if (!CollectionUtils.isEmpty(goodsLists)) {
            goodsLists.forEach(goods -> {
                goodsMapper.insertSelective(goods);
            });
            return true;
        }
        return false;
    }

    //删除商品
    @Override
    public ReturnResult delCommodity(GoodsVo goodsVo) {
        Employee employee = new Employee();
        if (powerUtils.JudgeDepartment(employee) == 3) {
            if (powerUtils.JudgePower(employee) == 2) {
                Goods goods = Goods.builder().build();
                goodsMapper.deleteByPrimaryKey(goodsVo.getGId());
                BeanUtils.copyProperties(goodsVo, goods);
                return ReturnResultUtils.returnSuccess(MessageTrue.MESSAGE_SHANCHU_TRUE.getMsg());
            }
            return ReturnResultUtils.returnFail(MessageError.MESSAGE_QUANXIAN_ERROR.getCode(), MessageError.MESSAGE_QUANXIAN_ERROR.getMsg());
        }
        return ReturnResultUtils.returnFail(MessageError.MESSAGE_BUMEN_ERROR.getCode(), MessageError.MESSAGE_BUMEN_ERROR.getMsg());
    }

    /**
     * 更新商品
     *
     * @param goodsVo
     * @return
     */
    @Override
    public ReturnResult upCommodity(GoodsVo goodsVo) {
        Employee employee = new Employee();
        if (powerUtils.JudgeDepartment(employee) == 3) {
            Goods goods = Goods.builder().build();
            BeanUtils.copyProperties(goodsVo, goods);
            GoodsExample goodsExample = new GoodsExample();
            goodsExample.createCriteria().andGIdEqualTo(goodsVo.getGId());
            goodsMapper.updateByPrimaryKey(goods);
            return ReturnResultUtils.returnSuccess(MessageTrue.MESSAGE_UPDATE_TRUE.getMsg());
        }
        return ReturnResultUtils.returnFail(MessageError.MESSAGE_BUMEN_ERROR.getCode(), MessageError.MESSAGE_BUMEN_ERROR.getMsg());
    }

    /**
     * 查询商品
     *
     * @param goodsVo
     * @return
     */
    @Override
    public Goods showCommodity(GoodsVo goodsVo) {
        Goods goods = Goods.builder().build();
        BeanUtils.copyProperties(goodsVo, goods);
        //CE1 -CE2 转化成json格式输出
        Goods CE1 = goodsMapper.showByPrimaryKeyorName(goods);
        Goods CE2 = Goods.builder().build();
        if (CE1 != null) {
            String s = JSONObject.toJSONString(CE1);
            CE2 = JSONObject.parseObject(s, Goods.class);
        }
        return CE2;
    }


}
