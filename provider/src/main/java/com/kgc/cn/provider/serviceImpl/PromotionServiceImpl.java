package com.kgc.cn.provider.serviceImpl;

import com.alibaba.dubbo.config.annotation.Service;
import com.google.common.collect.Lists;
import com.kgc.cn.common.config.ReturnResult.ReturnResult;
import com.kgc.cn.common.dto.Goods;
import com.kgc.cn.common.dto.Promotion;
import com.kgc.cn.common.enums.PromotionEnum;
import com.kgc.cn.common.service.PromotionService;
import com.kgc.cn.common.util.ReturnResultUtils;
import com.kgc.cn.common.vo.PromotionVo;
import com.kgc.cn.provider.mapper.GoodsMapper;
import com.kgc.cn.provider.mapper.PromotionMapper;
import com.kgc.cn.provider.utils.TimingUtils;
import org.jboss.netty.util.internal.ReusableIterator;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class PromotionServiceImpl implements PromotionService {

    @Autowired
    private PromotionMapper promotionMapper;

    @Autowired
    private GoodsMapper goodsMapper;

    @Override
    public boolean addPromotionGoods(String discountGoodsId, double discount, String startTime, String endTime) {

        List<String> goodsList = Arrays.asList(discountGoodsId.split(","));
        List<String> result = goodsList.stream()
                .filter(good -> goodsMapper.selectByPrimaryKey(good) != null
                        && promotionMapper.selectByGoodsId(good) == null)
                .collect(Collectors.toList());

        result.forEach(good -> {
            Promotion promotion = Promotion.builder().gId(good).pPrice(getRealPrice(good, discount))
                    .pDiscount(discount).pStartTime(startTime).pEndTime(endTime).build();
            promotionMapper.insert(promotion);

        });
        return true;
    }

    @Override
    public boolean judge(double count, String endTime) {
        Integer time = Integer.parseInt(endTime.replace("-", ""));
        DateFormat format = new SimpleDateFormat("yyyyMMdd");
        Integer now = Integer.parseInt(format.format(new Date()));
        if (count > 0 && count < 1 && time > now) {
            return true;
        }
        return false;
    }

    //获取商品折扣价
    public double getRealPrice(String goodsId, double content) {
        double RealPrice = goodsMapper.selectByPrimaryKey(goodsId).getGPrice();
        return RealPrice * content;
    }

    /**
     * 根据设定时间删除促销商品
     *
     * @return
     */
    public ReturnResult delDisCommodity() {
        TimingUtils timingUtils = new TimingUtils();
        timingUtils.delDisCommodity();
        return null;
    }

    /**
     * 修改促销商品
     *
     * @param promotionVo
     * @return
     */
    @Override
    public ReturnResult updatePromotion(PromotionVo promotionVo) {
        Promotion promotion = Promotion.builder().build();
        BeanUtils.copyProperties(promotionVo, promotion);
        //判断促销商品是否存在
        if (null != promotionMapper.selectByPrimaryKey(promotion.getPId())) {
            if (promotionMapper.updateByPrimaryKey(promotion)) {
                return ReturnResultUtils.returnSuccess(PromotionEnum.PromotionTrue.PROMOTION_MESSAGE_TRUE.getMsg());
            }
            return ReturnResultUtils.returnFail(PromotionEnum.PromotionError.PROMOTION_MESSAGE_ERRPR2.getCode(),
                    PromotionEnum.PromotionError.PROMOTION_MESSAGE_ERRPR2.getMsg());
        }
        return ReturnResultUtils.returnFail(PromotionEnum.PromotionError.PROMOTION_MESSAGE_ERROR1.getCode(),
                PromotionEnum.PromotionError.PROMOTION_MESSAGE_ERROR1.getMsg());
    }

    /**
     * 促销商品查询
     *
     * @return
     */
    @Override
    public ReturnResult<List<PromotionVo>> selPromotion(String name) {
        //模糊查询促销商品是否存在
        if (!goodsMapper.selectByName(name).isEmpty()) {
            List<PromotionVo> promotionList = Lists.newArrayList();
            List<Goods> goodsList = goodsMapper.selectByName(name);
            goodsList.forEach(goods -> {
                PromotionVo promotionVo = new PromotionVo();
                String gid = goods.getGId();
                System.out.println(gid);
                Promotion promotion = promotionMapper.selectByGoodsId(gid);
                BeanUtils.copyProperties(promotion, promotionVo);
                String goodsName = goodsMapper.selectByPrimaryKey(promotion.getGId()).getGName();
                promotionVo.setGName(goodsName);
                promotionList.add(promotionVo);
            });
            return ReturnResultUtils.returnSuccess(promotionList);
        }
        return ReturnResultUtils.returnFail(PromotionEnum.PromotionError.PROMOTION_MESSAGE_ERROR1.getCode(),
                PromotionEnum.PromotionError.PROMOTION_MESSAGE_ERROR1.getMsg());
    }

}