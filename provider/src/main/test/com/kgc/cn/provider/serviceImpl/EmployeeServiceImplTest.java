package com.kgc.cn.provider.serviceImpl;

import com.kgc.cn.provider.mapper.EmployeeMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeServiceImplTest {
    @Autowired
    private EmployeeMapper employeeMapper;

    @Test
    public void seEmployeeByPhoneOrEmail() {
        System.out.println(employeeMapper.selectByPhoneOrEmail("1"));
    }
}