package com.kgc.cn.common.service;

import com.kgc.cn.common.config.ReturnResult.ReturnResult;
import com.kgc.cn.common.dto.Goods;
import com.kgc.cn.common.vo.GoodsVo;

/**
 * Commodity商品信息
 * Created by swj on 2019/12/13.
 */

public interface GoodsService {

    /**
     * excel读取录入商品
     *
     * @return
     * @throws Exception
     */
    boolean excelAddGoods() throws Exception;

    /**
     * 删除商品
     *
     * @param goodsVo
     * @return
     */
    ReturnResult delCommodity(GoodsVo goodsVo);

    /**
     * 修改商品信息
     *
     * @param goodsVo
     * @return
     */
    ReturnResult upCommodity(GoodsVo goodsVo);

    /**
     * 查询商品id/商品名/类型
     *
     * @param goodsVo
     * @return
     */
    Goods showCommodity(GoodsVo goodsVo);

}
