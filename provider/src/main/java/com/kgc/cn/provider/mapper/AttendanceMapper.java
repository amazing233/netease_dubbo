package com.kgc.cn.provider.mapper;

import com.kgc.cn.common.dto.Attendance;
import com.kgc.cn.common.dto.AttendanceExample;
import com.kgc.cn.common.dto.EmployeeParam;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AttendanceMapper {
    long countByExample(AttendanceExample example);

    int deleteByExample(AttendanceExample example);

    int deleteByPrimaryKey(Long aId);

    int insert(Attendance record);

    int insertSelective(Attendance record);

    List<Attendance> selectByExample(AttendanceExample example);

    Attendance selectByPrimaryKey(Long aId);

    int updateByExampleSelective(@Param("record") Attendance record, @Param("example") AttendanceExample example);

    int updateByExample(@Param("record") Attendance record, @Param("example") AttendanceExample example);

    int updateByPrimaryKeySelective(Attendance record);

    int updateByPrimaryKey(Attendance record);

    /**
     * 根据日期查询未出勤的员工
     * 展示姓名，员工id，部门信息
     *
     * @return
     */
    List<EmployeeParam> searchNotAttendance(@Param("dateStr") String dateStr);

    /**
     * 当前员工的出勤天数
     *
     * @param eId
     * @param dateStr
     * @return
     */
    int daysOfAttendance(@Param("eId") String eId, @Param("dateStr") String dateStr);
}