package com.kgc.cn.common.util;


import com.kgc.cn.common.config.ReturnResult.ReturnResult;


public class ReturnResultUtils {

    /***
     * 成功 不带数据
     * @return
     */
    public static ReturnResult returnSuccess() {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(200);
        returnResult.setMessage("success");
        return returnResult;
    }

    /***
     * 成功 不带数据
     * @return
     */
    public static ReturnResult returnSuccess(Object data) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setMessage("success");
        returnResult.setCode(200);
        returnResult.setData(data);
        return returnResult;
    }

    /***
     * 失败
     * @return
     */
    public static ReturnResult returnFail(Integer code, String message) {
        ReturnResult returnResult = new ReturnResult();
        returnResult.setCode(code);
        returnResult.setMessage(message);
        return returnResult;
    }
}
