package com.kgc.cn.common.vo;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class DepartmentVo implements Serializable {
    private Long dId;

    private String dName;


}