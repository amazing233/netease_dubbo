package com.kgc.cn.common.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class Promotion implements Serializable {
    private Long pId;

    private String gId;

    private Double pPrice;

    private Double pDiscount;

    private String pStartTime;

    private String pEndTime;


}