package com.kgc.cn.common.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Attendance {
    private Long aId;

    private String eId;

    private String aTime;


}