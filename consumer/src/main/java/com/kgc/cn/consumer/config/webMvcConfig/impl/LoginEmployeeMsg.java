package com.kgc.cn.consumer.config.webMvcConfig.impl;

import com.alibaba.fastjson.JSONObject;
import com.kgc.cn.common.dto.Employee;
import com.kgc.cn.consumer.config.webMvcConfig.LoginEmployee;
import org.springframework.core.MethodParameter;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

public class LoginEmployeeMsg implements HandlerMethodArgumentResolver {
    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterType().isAssignableFrom(Employee.class)
                && parameter.hasParameterAnnotation(LoginEmployee.class);
    }

    @Override
    public Object resolveArgument(MethodParameter methodParameter, ModelAndViewContainer modelAndViewContainer, NativeWebRequest nativeWebRequest, WebDataBinderFactory webDataBinderFactory) throws Exception {
        String employeeJsonStr = (String) nativeWebRequest.getAttribute("employeeJsonStr", RequestAttributes.SCOPE_REQUEST);
        Employee employee = JSONObject.parseObject(employeeJsonStr, Employee.class);
        if (!ObjectUtils.isEmpty(employee)) {
            return employee;
        }
        return null;
    }
}
