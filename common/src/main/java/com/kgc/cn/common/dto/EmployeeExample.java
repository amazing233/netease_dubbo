package com.kgc.cn.common.dto;

import java.util.ArrayList;
import java.util.List;

public class EmployeeExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public EmployeeExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andEIdIsNull() {
            addCriterion("e_id is null");
            return (Criteria) this;
        }

        public Criteria andEIdIsNotNull() {
            addCriterion("e_id is not null");
            return (Criteria) this;
        }

        public Criteria andEIdEqualTo(String value) {
            addCriterion("e_id =", value, "eId");
            return (Criteria) this;
        }

        public Criteria andEIdNotEqualTo(String value) {
            addCriterion("e_id <>", value, "eId");
            return (Criteria) this;
        }

        public Criteria andEIdGreaterThan(String value) {
            addCriterion("e_id >", value, "eId");
            return (Criteria) this;
        }

        public Criteria andEIdGreaterThanOrEqualTo(String value) {
            addCriterion("e_id >=", value, "eId");
            return (Criteria) this;
        }

        public Criteria andEIdLessThan(String value) {
            addCriterion("e_id <", value, "eId");
            return (Criteria) this;
        }

        public Criteria andEIdLessThanOrEqualTo(String value) {
            addCriterion("e_id <=", value, "eId");
            return (Criteria) this;
        }

        public Criteria andEIdLike(String value) {
            addCriterion("e_id like", value, "eId");
            return (Criteria) this;
        }

        public Criteria andEIdNotLike(String value) {
            addCriterion("e_id not like", value, "eId");
            return (Criteria) this;
        }

        public Criteria andEIdIn(List<String> values) {
            addCriterion("e_id in", values, "eId");
            return (Criteria) this;
        }

        public Criteria andEIdNotIn(List<String> values) {
            addCriterion("e_id not in", values, "eId");
            return (Criteria) this;
        }

        public Criteria andEIdBetween(String value1, String value2) {
            addCriterion("e_id between", value1, value2, "eId");
            return (Criteria) this;
        }

        public Criteria andEIdNotBetween(String value1, String value2) {
            addCriterion("e_id not between", value1, value2, "eId");
            return (Criteria) this;
        }

        public Criteria andENameIsNull() {
            addCriterion("e_name is null");
            return (Criteria) this;
        }

        public Criteria andENameIsNotNull() {
            addCriterion("e_name is not null");
            return (Criteria) this;
        }

        public Criteria andENameEqualTo(String value) {
            addCriterion("e_name =", value, "eName");
            return (Criteria) this;
        }

        public Criteria andENameNotEqualTo(String value) {
            addCriterion("e_name <>", value, "eName");
            return (Criteria) this;
        }

        public Criteria andENameGreaterThan(String value) {
            addCriterion("e_name >", value, "eName");
            return (Criteria) this;
        }

        public Criteria andENameGreaterThanOrEqualTo(String value) {
            addCriterion("e_name >=", value, "eName");
            return (Criteria) this;
        }

        public Criteria andENameLessThan(String value) {
            addCriterion("e_name <", value, "eName");
            return (Criteria) this;
        }

        public Criteria andENameLessThanOrEqualTo(String value) {
            addCriterion("e_name <=", value, "eName");
            return (Criteria) this;
        }

        public Criteria andENameLike(String value) {
            addCriterion("e_name like", value, "eName");
            return (Criteria) this;
        }

        public Criteria andENameNotLike(String value) {
            addCriterion("e_name not like", value, "eName");
            return (Criteria) this;
        }

        public Criteria andENameIn(List<String> values) {
            addCriterion("e_name in", values, "eName");
            return (Criteria) this;
        }

        public Criteria andENameNotIn(List<String> values) {
            addCriterion("e_name not in", values, "eName");
            return (Criteria) this;
        }

        public Criteria andENameBetween(String value1, String value2) {
            addCriterion("e_name between", value1, value2, "eName");
            return (Criteria) this;
        }

        public Criteria andENameNotBetween(String value1, String value2) {
            addCriterion("e_name not between", value1, value2, "eName");
            return (Criteria) this;
        }

        public Criteria andEAgeIsNull() {
            addCriterion("e_age is null");
            return (Criteria) this;
        }

        public Criteria andEAgeIsNotNull() {
            addCriterion("e_age is not null");
            return (Criteria) this;
        }

        public Criteria andEAgeEqualTo(Integer value) {
            addCriterion("e_age =", value, "eAge");
            return (Criteria) this;
        }

        public Criteria andEAgeNotEqualTo(Integer value) {
            addCriterion("e_age <>", value, "eAge");
            return (Criteria) this;
        }

        public Criteria andEAgeGreaterThan(Integer value) {
            addCriterion("e_age >", value, "eAge");
            return (Criteria) this;
        }

        public Criteria andEAgeGreaterThanOrEqualTo(Integer value) {
            addCriterion("e_age >=", value, "eAge");
            return (Criteria) this;
        }

        public Criteria andEAgeLessThan(Integer value) {
            addCriterion("e_age <", value, "eAge");
            return (Criteria) this;
        }

        public Criteria andEAgeLessThanOrEqualTo(Integer value) {
            addCriterion("e_age <=", value, "eAge");
            return (Criteria) this;
        }

        public Criteria andEAgeIn(List<Integer> values) {
            addCriterion("e_age in", values, "eAge");
            return (Criteria) this;
        }

        public Criteria andEAgeNotIn(List<Integer> values) {
            addCriterion("e_age not in", values, "eAge");
            return (Criteria) this;
        }

        public Criteria andEAgeBetween(Integer value1, Integer value2) {
            addCriterion("e_age between", value1, value2, "eAge");
            return (Criteria) this;
        }

        public Criteria andEAgeNotBetween(Integer value1, Integer value2) {
            addCriterion("e_age not between", value1, value2, "eAge");
            return (Criteria) this;
        }

        public Criteria andESexIsNull() {
            addCriterion("e_sex is null");
            return (Criteria) this;
        }

        public Criteria andESexIsNotNull() {
            addCriterion("e_sex is not null");
            return (Criteria) this;
        }

        public Criteria andESexEqualTo(Integer value) {
            addCriterion("e_sex =", value, "eSex");
            return (Criteria) this;
        }

        public Criteria andESexNotEqualTo(Integer value) {
            addCriterion("e_sex <>", value, "eSex");
            return (Criteria) this;
        }

        public Criteria andESexGreaterThan(Integer value) {
            addCriterion("e_sex >", value, "eSex");
            return (Criteria) this;
        }

        public Criteria andESexGreaterThanOrEqualTo(Integer value) {
            addCriterion("e_sex >=", value, "eSex");
            return (Criteria) this;
        }

        public Criteria andESexLessThan(Integer value) {
            addCriterion("e_sex <", value, "eSex");
            return (Criteria) this;
        }

        public Criteria andESexLessThanOrEqualTo(Integer value) {
            addCriterion("e_sex <=", value, "eSex");
            return (Criteria) this;
        }

        public Criteria andESexIn(List<Integer> values) {
            addCriterion("e_sex in", values, "eSex");
            return (Criteria) this;
        }

        public Criteria andESexNotIn(List<Integer> values) {
            addCriterion("e_sex not in", values, "eSex");
            return (Criteria) this;
        }

        public Criteria andESexBetween(Integer value1, Integer value2) {
            addCriterion("e_sex between", value1, value2, "eSex");
            return (Criteria) this;
        }

        public Criteria andESexNotBetween(Integer value1, Integer value2) {
            addCriterion("e_sex not between", value1, value2, "eSex");
            return (Criteria) this;
        }

        public Criteria andEPhoneIsNull() {
            addCriterion("e_phone is null");
            return (Criteria) this;
        }

        public Criteria andEPhoneIsNotNull() {
            addCriterion("e_phone is not null");
            return (Criteria) this;
        }

        public Criteria andEPhoneEqualTo(String value) {
            addCriterion("e_phone =", value, "ePhone");
            return (Criteria) this;
        }

        public Criteria andEPhoneNotEqualTo(String value) {
            addCriterion("e_phone <>", value, "ePhone");
            return (Criteria) this;
        }

        public Criteria andEPhoneGreaterThan(String value) {
            addCriterion("e_phone >", value, "ePhone");
            return (Criteria) this;
        }

        public Criteria andEPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("e_phone >=", value, "ePhone");
            return (Criteria) this;
        }

        public Criteria andEPhoneLessThan(String value) {
            addCriterion("e_phone <", value, "ePhone");
            return (Criteria) this;
        }

        public Criteria andEPhoneLessThanOrEqualTo(String value) {
            addCriterion("e_phone <=", value, "ePhone");
            return (Criteria) this;
        }

        public Criteria andEPhoneLike(String value) {
            addCriterion("e_phone like", value, "ePhone");
            return (Criteria) this;
        }

        public Criteria andEPhoneNotLike(String value) {
            addCriterion("e_phone not like", value, "ePhone");
            return (Criteria) this;
        }

        public Criteria andEPhoneIn(List<String> values) {
            addCriterion("e_phone in", values, "ePhone");
            return (Criteria) this;
        }

        public Criteria andEPhoneNotIn(List<String> values) {
            addCriterion("e_phone not in", values, "ePhone");
            return (Criteria) this;
        }

        public Criteria andEPhoneBetween(String value1, String value2) {
            addCriterion("e_phone between", value1, value2, "ePhone");
            return (Criteria) this;
        }

        public Criteria andEPhoneNotBetween(String value1, String value2) {
            addCriterion("e_phone not between", value1, value2, "ePhone");
            return (Criteria) this;
        }

        public Criteria andEPasswordIsNull() {
            addCriterion("e_password is null");
            return (Criteria) this;
        }

        public Criteria andEPasswordIsNotNull() {
            addCriterion("e_password is not null");
            return (Criteria) this;
        }

        public Criteria andEPasswordEqualTo(String value) {
            addCriterion("e_password =", value, "ePassword");
            return (Criteria) this;
        }

        public Criteria andEPasswordNotEqualTo(String value) {
            addCriterion("e_password <>", value, "ePassword");
            return (Criteria) this;
        }

        public Criteria andEPasswordGreaterThan(String value) {
            addCriterion("e_password >", value, "ePassword");
            return (Criteria) this;
        }

        public Criteria andEPasswordGreaterThanOrEqualTo(String value) {
            addCriterion("e_password >=", value, "ePassword");
            return (Criteria) this;
        }

        public Criteria andEPasswordLessThan(String value) {
            addCriterion("e_password <", value, "ePassword");
            return (Criteria) this;
        }

        public Criteria andEPasswordLessThanOrEqualTo(String value) {
            addCriterion("e_password <=", value, "ePassword");
            return (Criteria) this;
        }

        public Criteria andEPasswordLike(String value) {
            addCriterion("e_password like", value, "ePassword");
            return (Criteria) this;
        }

        public Criteria andEPasswordNotLike(String value) {
            addCriterion("e_password not like", value, "ePassword");
            return (Criteria) this;
        }

        public Criteria andEPasswordIn(List<String> values) {
            addCriterion("e_password in", values, "ePassword");
            return (Criteria) this;
        }

        public Criteria andEPasswordNotIn(List<String> values) {
            addCriterion("e_password not in", values, "ePassword");
            return (Criteria) this;
        }

        public Criteria andEPasswordBetween(String value1, String value2) {
            addCriterion("e_password between", value1, value2, "ePassword");
            return (Criteria) this;
        }

        public Criteria andEPasswordNotBetween(String value1, String value2) {
            addCriterion("e_password not between", value1, value2, "ePassword");
            return (Criteria) this;
        }

        public Criteria andEEmailIsNull() {
            addCriterion("e_email is null");
            return (Criteria) this;
        }

        public Criteria andEEmailIsNotNull() {
            addCriterion("e_email is not null");
            return (Criteria) this;
        }

        public Criteria andEEmailEqualTo(String value) {
            addCriterion("e_email =", value, "eEmail");
            return (Criteria) this;
        }

        public Criteria andEEmailNotEqualTo(String value) {
            addCriterion("e_email <>", value, "eEmail");
            return (Criteria) this;
        }

        public Criteria andEEmailGreaterThan(String value) {
            addCriterion("e_email >", value, "eEmail");
            return (Criteria) this;
        }

        public Criteria andEEmailGreaterThanOrEqualTo(String value) {
            addCriterion("e_email >=", value, "eEmail");
            return (Criteria) this;
        }

        public Criteria andEEmailLessThan(String value) {
            addCriterion("e_email <", value, "eEmail");
            return (Criteria) this;
        }

        public Criteria andEEmailLessThanOrEqualTo(String value) {
            addCriterion("e_email <=", value, "eEmail");
            return (Criteria) this;
        }

        public Criteria andEEmailLike(String value) {
            addCriterion("e_email like", value, "eEmail");
            return (Criteria) this;
        }

        public Criteria andEEmailNotLike(String value) {
            addCriterion("e_email not like", value, "eEmail");
            return (Criteria) this;
        }

        public Criteria andEEmailIn(List<String> values) {
            addCriterion("e_email in", values, "eEmail");
            return (Criteria) this;
        }

        public Criteria andEEmailNotIn(List<String> values) {
            addCriterion("e_email not in", values, "eEmail");
            return (Criteria) this;
        }

        public Criteria andEEmailBetween(String value1, String value2) {
            addCriterion("e_email between", value1, value2, "eEmail");
            return (Criteria) this;
        }

        public Criteria andEEmailNotBetween(String value1, String value2) {
            addCriterion("e_email not between", value1, value2, "eEmail");
            return (Criteria) this;
        }

        public Criteria andESalaryIsNull() {
            addCriterion("e_salary is null");
            return (Criteria) this;
        }

        public Criteria andESalaryIsNotNull() {
            addCriterion("e_salary is not null");
            return (Criteria) this;
        }

        public Criteria andESalaryEqualTo(Double value) {
            addCriterion("e_salary =", value, "eSalary");
            return (Criteria) this;
        }

        public Criteria andESalaryNotEqualTo(Double value) {
            addCriterion("e_salary <>", value, "eSalary");
            return (Criteria) this;
        }

        public Criteria andESalaryGreaterThan(Double value) {
            addCriterion("e_salary >", value, "eSalary");
            return (Criteria) this;
        }

        public Criteria andESalaryGreaterThanOrEqualTo(Double value) {
            addCriterion("e_salary >=", value, "eSalary");
            return (Criteria) this;
        }

        public Criteria andESalaryLessThan(Double value) {
            addCriterion("e_salary <", value, "eSalary");
            return (Criteria) this;
        }

        public Criteria andESalaryLessThanOrEqualTo(Double value) {
            addCriterion("e_salary <=", value, "eSalary");
            return (Criteria) this;
        }

        public Criteria andESalaryIn(List<Double> values) {
            addCriterion("e_salary in", values, "eSalary");
            return (Criteria) this;
        }

        public Criteria andESalaryNotIn(List<Double> values) {
            addCriterion("e_salary not in", values, "eSalary");
            return (Criteria) this;
        }

        public Criteria andESalaryBetween(Double value1, Double value2) {
            addCriterion("e_salary between", value1, value2, "eSalary");
            return (Criteria) this;
        }

        public Criteria andESalaryNotBetween(Double value1, Double value2) {
            addCriterion("e_salary not between", value1, value2, "eSalary");
            return (Criteria) this;
        }

        public Criteria andEPowerIsNull() {
            addCriterion("e_power is null");
            return (Criteria) this;
        }

        public Criteria andEPowerIsNotNull() {
            addCriterion("e_power is not null");
            return (Criteria) this;
        }

        public Criteria andEPowerEqualTo(Integer value) {
            addCriterion("e_power =", value, "ePower");
            return (Criteria) this;
        }

        public Criteria andEPowerNotEqualTo(Integer value) {
            addCriterion("e_power <>", value, "ePower");
            return (Criteria) this;
        }

        public Criteria andEPowerGreaterThan(Integer value) {
            addCriterion("e_power >", value, "ePower");
            return (Criteria) this;
        }

        public Criteria andEPowerGreaterThanOrEqualTo(Integer value) {
            addCriterion("e_power >=", value, "ePower");
            return (Criteria) this;
        }

        public Criteria andEPowerLessThan(Integer value) {
            addCriterion("e_power <", value, "ePower");
            return (Criteria) this;
        }

        public Criteria andEPowerLessThanOrEqualTo(Integer value) {
            addCriterion("e_power <=", value, "ePower");
            return (Criteria) this;
        }

        public Criteria andEPowerIn(List<Integer> values) {
            addCriterion("e_power in", values, "ePower");
            return (Criteria) this;
        }

        public Criteria andEPowerNotIn(List<Integer> values) {
            addCriterion("e_power not in", values, "ePower");
            return (Criteria) this;
        }

        public Criteria andEPowerBetween(Integer value1, Integer value2) {
            addCriterion("e_power between", value1, value2, "ePower");
            return (Criteria) this;
        }

        public Criteria andEPowerNotBetween(Integer value1, Integer value2) {
            addCriterion("e_power not between", value1, value2, "ePower");
            return (Criteria) this;
        }

        public Criteria andEJoinTimeIsNull() {
            addCriterion("e_join_time is null");
            return (Criteria) this;
        }

        public Criteria andEJoinTimeIsNotNull() {
            addCriterion("e_join_time is not null");
            return (Criteria) this;
        }

        public Criteria andEJoinTimeEqualTo(String value) {
            addCriterion("e_join_time =", value, "eJoinTime");
            return (Criteria) this;
        }

        public Criteria andEJoinTimeNotEqualTo(String value) {
            addCriterion("e_join_time <>", value, "eJoinTime");
            return (Criteria) this;
        }

        public Criteria andEJoinTimeGreaterThan(String value) {
            addCriterion("e_join_time >", value, "eJoinTime");
            return (Criteria) this;
        }

        public Criteria andEJoinTimeGreaterThanOrEqualTo(String value) {
            addCriterion("e_join_time >=", value, "eJoinTime");
            return (Criteria) this;
        }

        public Criteria andEJoinTimeLessThan(String value) {
            addCriterion("e_join_time <", value, "eJoinTime");
            return (Criteria) this;
        }

        public Criteria andEJoinTimeLessThanOrEqualTo(String value) {
            addCriterion("e_join_time <=", value, "eJoinTime");
            return (Criteria) this;
        }

        public Criteria andEJoinTimeLike(String value) {
            addCriterion("e_join_time like", value, "eJoinTime");
            return (Criteria) this;
        }

        public Criteria andEJoinTimeNotLike(String value) {
            addCriterion("e_join_time not like", value, "eJoinTime");
            return (Criteria) this;
        }

        public Criteria andEJoinTimeIn(List<String> values) {
            addCriterion("e_join_time in", values, "eJoinTime");
            return (Criteria) this;
        }

        public Criteria andEJoinTimeNotIn(List<String> values) {
            addCriterion("e_join_time not in", values, "eJoinTime");
            return (Criteria) this;
        }

        public Criteria andEJoinTimeBetween(String value1, String value2) {
            addCriterion("e_join_time between", value1, value2, "eJoinTime");
            return (Criteria) this;
        }

        public Criteria andEJoinTimeNotBetween(String value1, String value2) {
            addCriterion("e_join_time not between", value1, value2, "eJoinTime");
            return (Criteria) this;
        }

        public Criteria andDIdIsNull() {
            addCriterion("d_id is null");
            return (Criteria) this;
        }

        public Criteria andDIdIsNotNull() {
            addCriterion("d_id is not null");
            return (Criteria) this;
        }

        public Criteria andDIdEqualTo(Integer value) {
            addCriterion("d_id =", value, "dId");
            return (Criteria) this;
        }

        public Criteria andDIdNotEqualTo(Integer value) {
            addCriterion("d_id <>", value, "dId");
            return (Criteria) this;
        }

        public Criteria andDIdGreaterThan(Integer value) {
            addCriterion("d_id >", value, "dId");
            return (Criteria) this;
        }

        public Criteria andDIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("d_id >=", value, "dId");
            return (Criteria) this;
        }

        public Criteria andDIdLessThan(Integer value) {
            addCriterion("d_id <", value, "dId");
            return (Criteria) this;
        }

        public Criteria andDIdLessThanOrEqualTo(Integer value) {
            addCriterion("d_id <=", value, "dId");
            return (Criteria) this;
        }

        public Criteria andDIdIn(List<Integer> values) {
            addCriterion("d_id in", values, "dId");
            return (Criteria) this;
        }

        public Criteria andDIdNotIn(List<Integer> values) {
            addCriterion("d_id not in", values, "dId");
            return (Criteria) this;
        }

        public Criteria andDIdBetween(Integer value1, Integer value2) {
            addCriterion("d_id between", value1, value2, "dId");
            return (Criteria) this;
        }

        public Criteria andDIdNotBetween(Integer value1, Integer value2) {
            addCriterion("d_id not between", value1, value2, "dId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}