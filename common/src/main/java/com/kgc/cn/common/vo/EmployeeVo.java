package com.kgc.cn.common.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class EmployeeVo implements Serializable {
    private String eId;

    private String eName;

    private Integer eAge;

    private Integer eSex;

    private String ePhone;

    private String ePassword;

    private String eEmail;

    private Double eSalary;

    private Integer ePower;

    private String eJoinTime;

    private Integer dId;

}