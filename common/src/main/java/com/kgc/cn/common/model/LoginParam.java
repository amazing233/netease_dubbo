package com.kgc.cn.common.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class LoginParam implements Serializable {

    private String phoneOrEmail;

    private String password;


}
