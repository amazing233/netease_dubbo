package com.kgc.cn.provider.serviceImpl;

import com.alibaba.dubbo.config.annotation.Service;
import com.kgc.cn.common.dto.Attendance;
import com.kgc.cn.common.dto.EmployeeParam;
import com.kgc.cn.common.service.AttendanceService;
import com.kgc.cn.common.vo.EmployeeParamVo;
import com.kgc.cn.provider.mapper.AttendanceMapper;
import com.kgc.cn.provider.mapper.EmployeeMapper;
import org.apache.curator.shaded.com.google.common.collect.Lists;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class AttendanceImpl implements AttendanceService {

    @Autowired
    private AttendanceMapper attendanceMapper;
    @Autowired
    private EmployeeMapper employeeMapper;

    /**
     * 出勤打卡
     *
     * @param eId
     * @return
     */
    @Override
    public boolean insertAttendance(String eId) {
        Date date = new Date();
        String dateFormat = new SimpleDateFormat("yyyy-MM-dd").format(date);
        Attendance attendance = Attendance.builder().eId(eId).aTime(dateFormat).build();
        int num = attendanceMapper.insertSelective(attendance);
        if (num == 1) {
            return true;
        }
        return false;
    }

    /**
     * 查询所有的未出勤的员工根据日期
     *
     * @return
     */
    @Override
    public List<EmployeeParamVo> searchNoAttendance(String dateStr) {
        List<EmployeeParam> employeeParamList = attendanceMapper.searchNotAttendance(dateStr);
        List<EmployeeParamVo> employeeParamVoList = Lists.newArrayList();
        employeeParamList.forEach(employeeParam -> {
            EmployeeParamVo employeeParamVo = new EmployeeParamVo();
            BeanUtils.copyProperties(employeeParam, employeeParamVo);
            employeeParamVoList.add(employeeParamVo);
        });
        return employeeParamVoList;
    }

    /**
     * 当月当前员工的出勤天数
     *
     * @param eId
     * @return
     */
    @Override
    public int daysOfAttendance(String eId) {
        String dateStr = new SimpleDateFormat("yyyy-MM").format(new Date());
        int num = attendanceMapper.daysOfAttendance(eId, dateStr);
        return num;
    }
}
