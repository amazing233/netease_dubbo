package com.kgc.cn.common.util;

import java.util.UUID;

public class UuidUtil {

    public static String randomUuId() {
        String uuId = UUID.randomUUID().toString().replace("-", "").substring(0, 6);
        return "@" + uuId;
    }
}
