package com.kgc.cn.consumer.model;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * Created by boot on 2019/12/14
 */
@ApiModel
public class ParamMsg implements Serializable {
    private static final long serialVersionUID = -8282278876792325845L;
    @ApiModelProperty("输入员工姓名")
    private String eName;
    @ApiModelProperty("输入员工年龄")
    private Integer eAge;
    @ApiModelProperty("输入员工性别（1-男，2-女，3-其他）")
    private Integer eSex;
    @ApiModelProperty("输入电话号码")
    private String ePhone;
    @ApiModelProperty("输入email")
    private String eEmail;

    public String geteName() {
        return eName;
    }

    public void seteName(String eName) {
        this.eName = eName;
    }

    public Integer geteAge() {
        return eAge;
    }

    public void seteAge(Integer eAge) {
        this.eAge = eAge;
    }

    public Integer geteSex() {
        return eSex;
    }

    public void seteSex(Integer eSex) {
        this.eSex = eSex;
    }

    public String getePhone() {
        return ePhone;
    }

    public void setePhone(String ePhone) {
        this.ePhone = ePhone;
    }

    public String geteEmail() {
        return eEmail;
    }

    public void seteEmail(String eEmail) {
        this.eEmail = eEmail;
    }
}
