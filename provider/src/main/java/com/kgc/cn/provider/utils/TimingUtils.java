package com.kgc.cn.provider.utils;

import com.kgc.cn.provider.mapper.PromotionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by swj on 2019/12/16.
 */
@Component
public class TimingUtils {

    @Autowired
    private PromotionMapper promotionMapper;

    @Scheduled(cron = "0 0 0 1 * ?")
    public void delDisCommodity() {
        promotionMapper.delByTimeOut();
        System.out.println("删除成功");
    }


}