package com.kgc.cn.common.service;


import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.kgc.cn.common.config.ReturnResult.ReturnResult;
import com.kgc.cn.common.dto.Employee;
import com.kgc.cn.common.model.LoginParam;
import com.kgc.cn.common.vo.EmployeeVo;

import java.util.List;

public interface EmployeeService {


    /**
     * 读取excel批量增加用户
     *
     * @return
     * @throws Exception
     */
    boolean excelAddCredits() throws Exception;

    /**
     * 根据员工的手机号或者邮箱以及密码
     * 登录切返回员工信息
     *
     * @param loginParam
     * @return
     */
    EmployeeVo seEmployeeByPhoneOrEmailLogin(LoginParam loginParam);

    /**
     * 根据员手机号
     * 邮箱判断有无员工
     *
     * @param str
     * @return
     */
    EmployeeVo seEmployeeByPhoneOrEmail(String str);

    /**
     * 根据员工的id或者姓名模糊查询员工。
     *
     * @param eStr
     * @return
     */
    PageInfo<EmployeeVo> searchEmployeeByIdOrName(int pageNum, int pageSize, String eStr);

    /**
     * 根据员工入职时间段查询员工
     *
     * @param startTime
     * @param endTime
     * @return
     */
    List<EmployeeVo> searchEmployeeByJTime(String startTime, String endTime);

    /**
     * 修改员工的信息
     *
     * @param employee
     * @return
     */
    ReturnResult updateEmployee(Employee employee);

    /**
     * 通过员工的id进行删除员工
     *
     * @param eId
     * @return
     */
    ReturnResult delEmployee(Employee employee, String eId);

    /**
     * 根据用户信息发送初始密码
     */
    void sendMailByMail();
}
