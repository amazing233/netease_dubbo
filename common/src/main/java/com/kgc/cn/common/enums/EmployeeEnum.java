package com.kgc.cn.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @admin Created by boot on 2019/12/16
 */
public interface EmployeeEnum {

    @Getter
    @AllArgsConstructor
    enum EmployeeError implements EmployeeEnum {
        EmployeeEnum_SHANCHUZHIYUAN1_ERROR(111, "您没有操作此功能的权限"),
        EmployeeEnum_SHANCHUZHIYUAN2_ERROR(112, "您的权限不够"),
        EmployeeEnum_GENGXINZHIYUAN_ERROR(113, "职员不存在"),
        EmployeeEnum_GENGXINZHIYUAN2_ERROR(114, "输入信息不符合规范");
        int code;
        String msg;
    }

    @Getter
    @AllArgsConstructor
    enum EmployeeTrue implements EmployeeEnum {
        EmployeeEnum_SHANCHUZHIYUAN_TRUE("删除职员成功"),
        EmployeeEnum_GENGXINZHIYUAN_TRUE("修改成功");
        String msg;
    }


}
