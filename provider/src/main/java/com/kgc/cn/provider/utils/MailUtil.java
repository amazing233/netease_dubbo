package com.kgc.cn.provider.utils;

import com.kgc.cn.common.enums.MailEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

/**
 * create by rap
 * 发送email工具类
 */
@Component
public class MailUtil {

    @Autowired
    private JavaMailSender javaMailSender;


    @Value("${spring.mail.username}")
    private String sendFrom;

    //发送文件
    public void sendMail(String sendTo, String text) {
        try {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setFrom(sendFrom);
            message.setTo(sendTo);
            message.setSubject(MailEnum.SendEmail.SEND_EMAIL.getSubject());
            message.setText(text);
            javaMailSender.send(message);
            System.out.println("发送成功！");
        } catch (Exception e) {
            System.out.println("发送简单文本文件-发生异常");
        }
    }
}
