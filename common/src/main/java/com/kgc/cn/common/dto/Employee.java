package com.kgc.cn.common.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Builder
@Data
public class Employee implements Serializable {
    private String eId;

    private String eName;

    private Integer eAge;

    private Integer eSex;

    private String ePhone;

    private String ePassword;

    private String eEmail;

    private Double eSalary;

    private Integer ePower;

    private String eJoinTime;

    private Integer dId;

    public Employee(String eId, String eName, Integer eAge, Integer eSex,
                    String ePhone, String ePassword, String eEmail, Double eSalary,
                    Integer ePower, String eJoinTime, Integer dId) {
        this.eId = eId;
        this.eName = eName;
        this.eAge = eAge;
        this.eSex = eSex;
        this.ePhone = ePhone;
        this.ePassword = ePassword;
        this.eEmail = eEmail;
        this.eSalary = eSalary;
        this.ePower = ePower;
        this.eJoinTime = eJoinTime;
        this.dId = dId;
    }

    public Employee() {
    }
}