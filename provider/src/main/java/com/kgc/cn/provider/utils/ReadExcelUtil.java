package com.kgc.cn.provider.utils;

import com.google.common.collect.Lists;
import com.kgc.cn.common.dto.Employee;
import com.kgc.cn.common.dto.Goods;
import com.kgc.cn.common.util.UuidUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * create by rap
 * 读取excel存储数据库工具类
 */
@Component
public class ReadExcelUtil {

    //获取员工表信息
    public static List<Employee> ReadEmployeeExcel() throws NullPointerException, IOException {
        File excelFile = new File("/Users/shaoyufei/未命名文件夹/表格.xlsx");
        XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(excelFile));
        XSSFSheet sheet = wb.getSheetAt(0);

        List<Employee> employeeLists = Lists.newArrayList();

        for (Row row : sheet) {
            if (row.getCell(0) != null) {
                //获得人员姓名
                String name = row.getCell(0).toString();

                //获得人员年龄
                Integer age = returnInteger(row.getCell(1).getNumericCellValue());

                //获得人员性别
                Integer sex = returnInteger(row.getCell(2).getNumericCellValue());

                //获得人员入职时间
                DecimalFormat phoneFormat = new DecimalFormat("#");
                Number number = row.getCell(3).getNumericCellValue();

                //获得人员电话号码
                String phone = phoneFormat.format(number);

                //获得人员初始密码
                String password = row.getCell(4).toString();
                String mdPassword = convertMD5(password);

                //获得人员的邮箱
                String email = row.getCell(5).toString();

                //获得人员的薪资
                double salary = row.getCell(6).getNumericCellValue();

                //获得人员权限
                Integer power = returnInteger(row.getCell(7).getNumericCellValue());

                //获得人员入职时间
                Date joinTime = row.getCell(8).getDateCellValue();
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                String date = format.format(joinTime);

                //获得部门id
                Integer dId = returnInteger(row.getCell(9).getNumericCellValue());

                //生成员工id
                String eId = dId + phone.substring(6, 11);

                //生成员工对象
                Employee employee = Employee.builder().eId(eId).eName(name).eAge(age)
                        .eSex(sex).ePhone(phone).ePassword(mdPassword).eEmail(email)
                        .eSalary(salary).ePower(power).eJoinTime(date).dId(dId).build();

                employeeLists.add(employee);
            } else {
                break;
            }
        }
        return employeeLists;
    }

    //获取商品表信息
    public static List<Goods> ReadGoodsExcel() throws NullPointerException, IOException {
        File excelFile = new File("/Users/shaoyufei/未命名文件夹/商品表.xlsx");
        XSSFWorkbook wb = new XSSFWorkbook(new FileInputStream(excelFile));
        XSSFSheet sheet = wb.getSheetAt(0);

        List<Goods> goodsLists = Lists.newArrayList();

        for (Row row : sheet) {
            if (row.getCell(0) != null) {
                //获得商品id
                String gId = row.getCell(0).toString();

                //获得商品名
                String name = row.getCell(1).toString();

                //获得商品价格
                double gPrice = row.getCell(2).getNumericCellValue();

                //获得商品类型
                String goodsType = row.getCell(3).toString();

                //获得商品描述
                String goodsContent = row.getCell(4).toString();

                //生成s商品对象
                Goods goods = Goods.builder().gId(gId + UuidUtil.randomUuId()).gName(name)
                        .gPrice(gPrice).gType(goodsType).gContent(goodsContent).build();

                goodsLists.add(goods);
            } else {
                break;
            }
        }
        return goodsLists;
    }

    public static Integer returnInteger(double num) {
        Integer useNum = new Integer(new Double(num).intValue());
        return useNum;
    }

    /**
     * 加密解密算法 执行一次加密，两次解密
     */
    public static String convertMD5(String inStr) {

        char[] a = inStr.toCharArray();
        for (int i = 0; i < a.length; i++) {
            a[i] = (char) (a[i] ^ 't');
        }
        String s = new String(a);
        return s;

    }

}