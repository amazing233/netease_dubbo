package com.kgc.cn.provider.mapper;

import com.kgc.cn.common.dto.Goods;
import com.kgc.cn.common.dto.GoodsExample;

import java.util.List;

import com.kgc.cn.common.dto.Promotion;
import org.apache.ibatis.annotations.Param;

public interface GoodsMapper {
    long countByExample(GoodsExample example);

    int deleteByExample(GoodsExample example);

    int deleteByPrimaryKey(String gId);

    int insert(Goods record);

    int insertSelective(Goods record);

    List<Goods> selectByExample(GoodsExample example);

    Goods selectByPrimaryKey(String gId);

    int updateByExampleSelective(@Param("record") Goods record, @Param("example") GoodsExample example);

    int updateByExample(@Param("record") Goods record, @Param("example") GoodsExample example);

    int updateByPrimaryKeySelective(Goods record);

    int updateByPrimaryKey(Goods record);

    Goods showByPrimaryKeyorName(@Param("goods") Goods goods);

    List<Goods> selectByName(@Param("name") String name);
}