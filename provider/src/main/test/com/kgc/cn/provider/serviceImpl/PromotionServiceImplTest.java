package com.kgc.cn.provider.serviceImpl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.kgc.cn.common.service.PromotionService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


/**
 * Created by boot on 2019/12/18
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class PromotionServiceImplTest {
    @Reference
    private PromotionService promotionService;

    @Test
    public void selPromotion() {
        System.out.println(promotionService.selPromotion("花"));

    }
}
