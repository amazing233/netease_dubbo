package com.kgc.cn.common.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class EmployeeParam implements Serializable {
    private String eId;

    private String eName;

    private String dName;
}
