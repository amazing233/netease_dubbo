package com.kgc.cn.consumer.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.kgc.cn.common.config.ReturnResult.ReturnResult;
import com.kgc.cn.common.dto.Employee;
import com.kgc.cn.common.enums.PromotionEnum;
import com.kgc.cn.common.service.PromotionService;
import com.kgc.cn.common.util.ReturnResultUtils;
import com.kgc.cn.common.vo.PromotionVo;
import com.kgc.cn.consumer.config.webMvcConfig.LoginEmployee;
import com.kgc.cn.consumer.config.webMvcConfig.LoginRequired;
import com.kgc.cn.consumer.util.ReturnPageUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.xmlbeans.impl.common.ValidatorListener;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

@Api(tags = "促销操作")
@RestController
@RequestMapping(value = "/promotion")
public class PromotionController {

    @Reference
    private PromotionService promotionService;

    @ApiOperation(value = "批量增加促销商品")
    @PostMapping(value = "/addPromotionGoods")
    @ApiImplicitParam(name = "discountGoodsId", value = "打折商品id", required = true, allowMultiple = true, dataType = "String", paramType = "query")
    public ReturnResult addPromotionGoods(@ApiParam(value = "打折商品id") @RequestParam String discountGoodsId, @ApiParam(value = "折扣") @RequestParam double disContent,
                                          @ApiParam(value = "开始时间") @RequestParam String startTime, @ApiParam(value = "结束时间") @RequestParam String endTime) {
        boolean flag = promotionService.judge(disContent, endTime);
        if (flag) {
            promotionService.addPromotionGoods(discountGoodsId, disContent, startTime, endTime);
            return ReturnResultUtils.returnSuccess(PromotionEnum.MakeDiscount.MAKE_SUCCESS);
        }
        return ReturnResultUtils.returnFail(PromotionEnum.MakeDiscount.MAKE_FAIL.getCode(), PromotionEnum.MakeDiscount.MAKE_FAIL.getMsg());
    }

    @ApiOperation("修改促销商品信息")
    @PostMapping("/updePromotion")
    @LoginRequired
    public ReturnResult updatePromotion(@LoginEmployee Employee employee, @Valid PromotionVo promotionVo) {
        return promotionService.updatePromotion(promotionVo);
    }

    @ApiOperation("促销商品信息查询")
    @GetMapping("/selectPromotion")
    public ReturnResult<List<PromotionVo>> selectPromotion(@ApiParam(value = "商品名（模糊查询）") @RequestParam(name = "输入商品名") String name) {
        return ReturnResultUtils.returnSuccess(promotionService.selPromotion(name));

    }
}
