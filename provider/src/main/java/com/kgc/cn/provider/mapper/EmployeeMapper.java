package com.kgc.cn.provider.mapper;

import com.kgc.cn.common.dto.Employee;
import com.kgc.cn.common.dto.EmployeeExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EmployeeMapper {
    long countByExample(EmployeeExample example);

    int deleteByExample(EmployeeExample example);

    int deleteByPrimaryKey(String eId);

    int insert(Employee record);

    int insertSelective(Employee record);

    List<Employee> selectByExample(EmployeeExample example);

    Employee selectByPrimaryKey(String eId);

    int updateByExampleSelective(@Param("record") Employee record, @Param("example") EmployeeExample example);

    int updateByExample(@Param("record") Employee record, @Param("example") EmployeeExample example);

    int updateByPrimaryKeySelective(Employee record);

    int updateByPrimaryKey(Employee record);

    int excelAddMsg(@Param("eId") String eid, @Param("eName") String eName, @Param("eAge") Integer eAge, @Param("eSex") Integer sex, @Param("ePhone") String ePhone, @Param("ePassword") String ePassword, @Param("eEmail") String eEmail, @Param("eSalary") double eSalary, @Param("ePower") Integer ePower, @Param("eJoinTime") String eJoinTime, @Param("dId") Integer did);

    /**
     * 通过手机号以及邮箱判断有无该用户
     *
     * @param str
     * @return
     */
    Employee selectByPhoneOrEmail(@Param("str") String str);

    /**
     * 通过手机号以及邮箱登录
     *
     * @param str
     * @param pwd
     * @return
     */
    Employee selectByPhoneOrEmailLogin(@Param("str") String str, @Param("pwd") String pwd);

    /**
     * 通过id，通过姓名模糊查询
     *
     * @param eStr
     * @return
     */
    List<Employee> selectByIdOrName(@Param("eStr") String eStr);

    /**
     * 通过入职时间段查询
     *
     * @param startTime
     * @param endTime
     * @return
     */
    List<Employee> searchEmployeeByJTime(@Param("startTime") String startTime, @Param("endTime") String endTime);

    /**
     * @return
     */
    List<Employee> selectAll();


}