package com.kgc.cn.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

public interface MailEnum {

    String getSendTo();

    String getSubject();

    String getTxt();

    String getName();

    @Getter
    @AllArgsConstructor
    enum SendEmail implements MailEnum {
        SEND_EMAIL("lisongjian163@163.com", "你的密码！", "123456", "/Users/shaoyufei/未命名文件夹/表格.xlsx", "表格");
        private String sendTo;
        private String subject;
        private String txt;
        private String path;
        private String name;
    }
}
