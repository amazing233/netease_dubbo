package com.kgc.cn.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

public interface PromotionEnum {

    int getCode();

    String getMsg();

    @Getter
    @AllArgsConstructor
    enum MakeDiscount implements PromotionEnum {
        MAKE_SUCCESS(200, "促销商品增加成功！"),
        MAKE_FAIL(201, "促销商品增加失败！");
        private int code;
        private String msg;
    }

    @Getter
    @AllArgsConstructor
    enum PromotionTrue implements PromotionEnum {
        PROMOTION_MESSAGE_TRUE(200, "促销商品信息修改成功");
        int code;
        String msg;
    }

    @Getter
    @AllArgsConstructor
    enum PromotionError implements PromotionEnum {
        PROMOTION_MESSAGE_ERROR1(204, "商品不存在"),
        PROMOTION_MESSAGE_ERRPR2(206, "促销商品信息修改失败");
        int code;
        String msg;
    }
}
